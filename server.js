require('dotenv').config()
const { PORT, HOST } = process.env

const fastify = require('fastify')({ logger: true })

fastify.register(require('./app'))

const start = async () => {
  try {
    await fastify.listen({ port: PORT || 8081, host: HOST || 'localhost' })
    fastify.log.info(`server listening on ${fastify.server.address().port}`)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()