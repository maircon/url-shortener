const REGEX_URL = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/
const REGEX_VALID_URL = /^(?:https?:\/\/)?(?:www\.)?/i

module.exports = { REGEX_URL, REGEX_VALID_URL }
