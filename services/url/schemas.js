const bodySchema = {
  body: {
    type: 'object',
    required: ['url'],
    properties: {
      url: { type: 'string' }
    }
  }
}
const paramsSchema = {
  params: {
    type: 'object',
    required: ['short_url'],
    properties: {
      short_url: { type: 'string' }
    }
  }
}
module.exports = {
  bodySchema,
  paramsSchema
}
