const urlServices = require('./service')
const { BASE_URL } = process.env
const { REGEX_URL, REGEX_VALID_URL } = require('../../utils/regex')

const { paramsSchema, bodySchema } = require('./schemas')
const DICTIONARY = '0123456789qazwsxedcrfvtgbyhnujmikolpQAZWSXEDCRFVTGBYHNUJMIKOLP'
const MAX_LENGTH = 10

module.exports = async function (fastify, opt) {
  fastify.get('/:short_url', { schema: paramsSchema }, findUrlHandler)
  fastify.post('/encurtador', { schema: bodySchema }, createShortUrlHandler)
  fastify.get('/list', {}, listHandler)
  fastify.delete('/all', {}, deleteAllHandler)
}

async function findUrlHandler (req, reply) {
  const { short_url } = req.params
  if (!short_url) throw new Error('Insira a URL corretamente')
  const url_result = await urlServices.findOne({ short_url })
  if (url_result) {
    const had_expired = new Date() > url_result.expires_on
    if (had_expired) {
      await urlServices.delete({ url_id: url_result._id })
      reply.code(404).send({ message: 'URL não existe' })
    } else {
      const full_url = `https://${url_result.url}`
      reply.redirect(full_url)
    }
  } 
  else {
    reply.code(404).send({ message: 'URL não existe' })
  }
}

async function createShortUrlHandler (req, reply) {
  const { url } = req.body
  if (REGEX_URL.test(url) === false) throw new Error('Insira uma URL válida')
  const url_replace =  url.replace(REGEX_VALID_URL, "")
  const url_result = await urlServices.findOne({ url: url_replace })
  const expires_on = generateExpirationDate()
  let result = {}
  if (url_result) {
    result = await urlServices.update({ url_id: url_result._id, expires_on })
  } else {
    result.error = true
    while (result.error) {
      const short_url = createShortUrl()
      result = await urlServices.create({ short_url, url: url_replace, expires_on })
    }
  }
  return formatShortUrl(result.short_url)
}

async function listHandler (req, reply) {
  const urls = await urlServices.findAll()
  const formated_urls = urls.map(a => `${BASE_URL}${a.short_url}`)
  return formated_urls
}

async function deleteAllHandler (req, reply) {
  await urlServices.deleteAll()
  return { message: 'Valores Removidos' }
}

function createShortUrl() {
  let random_length = generateRandomValue(MAX_LENGTH)
  if (random_length < 5) random_length += 5
  let word_array = ''
  for (let i = 1; i <= random_length; i++) {
    const random_number = generateRandomValue(DICTIONARY.length - 1)
    word_array += DICTIONARY[random_number]
  }
  return word_array
}

function generateRandomValue(max_value) {
  return Math.round((Math.random() * max_value))
}

function formatShortUrl(short_url) {
  return { newUrl: `${BASE_URL}${short_url}` }
}

function generateExpirationDate(days = 7) {
  const date = new Date()
  return date.setDate(date.getDate() + days)
}
