const mongoose = require('mongoose')
const Schema = mongoose.Schema

const urlSchema = new Schema({
  url: { type: String, required: true, unique: true },
  short_url: { type: String, required: true, unique: true },
  expires_on: { type: Date, required: true }
})

const Url = mongoose.model('Url', urlSchema)
module.exports = Url
