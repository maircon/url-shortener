const Url = require('./Model/Url')

/**
 * Finds one document by field.
 * @param body - { short_url, url }
 */
exports.findOne = async (body) => {
  try {
    return await Url.findOne(body)
  } catch (e) {
    throw e
  }
}
exports.create = async ({ short_url, url, expires_on, i }) => {
  try {
    const res = new Url({ short_url, url, expires_on })
    return await res.save()
  } catch (e) {
    if (e.code === 11000) {
      return { error: true }
    }
    throw e
  }
}

exports.delete = async ({ url_id }) => {
  try {
    await Url.findByIdAndDelete(url_id)
  } catch (e) {
    throw e
  }
}

exports.update = async ({ url_id, expires_on }) => {
  try {
    const response = await Url.findByIdAndUpdate(url_id,
      { expires_on },
      { new: true })
    return response
  } catch (e) {
    throw e
  }
}

exports.deleteAll = async () => {
  try {
    await Url.deleteMany({})
  } catch (e) {
    throw e
  }
}

exports.findAll = async () => {
  try {
    return await Url.find({})
  } catch (e) {
    throw e
  }
}
