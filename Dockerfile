FROM mhart/alpine-node:12.18

WORKDIR /home/url

# RUN mkdir /usr/src/url

COPY yarn.lock .

COPY package.json .

RUN yarn install

RUN yarn global add nodemon

COPY . .

EXPOSE 8081

# CMD [ "yarn", "dev"]