const { DB_USER, DB_PASSWORD, DB_URL } = process.env
const mongoose = require('mongoose');

const opt = { 
  auth: { user: DB_USER, password: DB_PASSWORD },
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true, 
  useFindAndModify: false 
}

mongoose.connect(DB_URL, opt);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log("CONECTADO COM SUCESSO NO BANCO")
});

module.exports = async function (fastify, opt) {
  fastify.register(require('./services/url/index'))
}
