## Introdução

Esse projeto foi desenvolvido para representar URLs de forma curta.

Sites como bitly, tinyurl são exemplos da funcionalidade.

Seu principal objetivo é tornar urls longas mais amigáveis

## Funcionamento

O projeto foi pensado levando em considerações os mínimos requisitos possíveis.

O foco do projeto é representar um endereço de URL por um código. Exemplo: **https://google.com.br** -> **https://URL_HOST/iUdF1m**

Apesar de possuir certos validadores de url, o projeto necessita da boa fé do usuário ao inserir um URL. Por exemplo, ao inserir "zsxdcfvqweytgwqy.com" o programa vai reconhecer como uma URL válida.

O usuário poderá enviar o endereço de URl no corpo da requisição, podendo utilizar: www, http, https ou nenhum(ex. google.com.br).

Assim que a chamada for realizada, o programa validará a URL, removerá os campos anteriores ao dominio e salvará no banco de dados a informação resultante.

Exemplo: **https://www.google.com.br** -> após tratamento será salvo no banco assim: **google.com.br**.

Utilizando essa metodologia é possivel não replicar um mesmo endereço para vários códigos.

## Como o Código é gerado

O código é gerado a partir de um diciónario de caracteres e do número máximo possível dos mesmos.

Com isso, é possível combinar 52 letras(Maiúsculas e Minúsculas) + 10 números(0..9), totalizando 62 valores.

Levando em consideração que o mínimo possível de caracteres são 5 e o máximo são 10, podemos gerar mais de bilhões de valores.

## Como instalar

### Utilizando Docker

**Requisitos**

Docker 19.03.12

Docker-compose 1.25.4

1. Crie o documento .env a partir do env-example

2. Preencha os variáveis de desenvolvimento

3. utilize o comando docker-compose up

4. Pronto!

### Sem o docker

**Requisitos**

1. node v12.16.3

2. npm 6.14.4

3. yarn 1.22.4

4. nodemon 2

Após confirmar, todos os requisitos, podemos continuar

1. yarn

2. Crie o documento .env a partir do env-example

3. Preencha os variáveis de desenvolvimento

4. yarn dev or yarn start

5. pronto


## Documentação de Rotas

https://documenter.getpostman.com/view/6559121/T17AkWr4?version=latest#f39de3e6-2f53-4cd3-afe1-331f31d36434

## Domínio Principal

https://m-url-shortener.herokuapp.com/

## Licença

Licença MIT